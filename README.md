# Fme::Shoryuken
This is a library intended to manage fme's enterprise bus.

## Installation
Add this line to your application's Gemfile:

```ruby
gem 'fme-enterprise_bus'
```

And then execute:
```bash
$ bundle
```

Or install it yourself as:
```bash
$ gem install fme-enterprise_bus
```

## Usage if making custom jobs to process messages
### initializer
fme-enterprise_bus.rb
```ruby
Fme::EnterpriseBus::Jobs.register_job(custom_job_klass, hash_of_attributes_a_message_needs_to_be_passed_to_said_job)
```

## License
The gem is available as open source under the terms of the [MIT License](https://opensource.org/licenses/MIT).
