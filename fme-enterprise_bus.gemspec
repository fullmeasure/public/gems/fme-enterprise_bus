$:.push File.expand_path('lib', __dir__)

# Maintain your gem's version:
require 'fme/enterprise_bus/version'

# Describe your gem and declare its dependencies:
Gem::Specification.new do |spec|
  spec.name        = 'fme-enterprise_bus'
  spec.version     = Fme::EnterpriseBus::VERSION
  spec.authors     = ['Full Measure Education']
  spec.email       = ['devops@fullmeasureed.com']
  spec.homepage    = 'https://gitlab.com/fullmeasure/public/gems/fme-enterprise_bus'
  spec.summary     = 'A ruby library for fme to communication between bound contexts'
  spec.description = 'A ruby library for fme to communication between bound contexts'
  spec.license     = 'MIT'

  spec.files = Dir["{app,config,db,lib}/**/*", "MIT-LICENSE", "Rakefile", "README.md"]

  spec.add_dependency 'activesupport', '~> 5'
  spec.add_dependency 'rails', '~> 5'
  spec.add_dependency 'propono', '~> 2'
  spec.add_dependency 'fme-shoryuken', '~> 0'
  spec.add_dependency 'active_attr', '~> 0'

  spec.add_development_dependency 'bundler', '~> 1'
  spec.add_development_dependency 'pry'
  spec.add_development_dependency 'pry-byebug'
  spec.add_development_dependency 'rspec', '~> 3'
  spec.add_development_dependency 'simplecov', '~> 0'
  spec.add_development_dependency 'webmock', '~> 3'
  spec.add_development_dependency 'rspec-rails', '~> 3'
  spec.add_development_dependency 'database_cleaner', '~> 1'
end
