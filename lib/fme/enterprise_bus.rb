# frozen_string_literal: true

require 'active_support'
require 'propono'
require 'rails'

module Propono
  class AwsConfig
    def aws_options
      {
      }
    end
  end
end

module Fme
  module EnterpriseBus
    extend ActiveSupport::Autoload

    autoload :VERSION
    autoload :Railtie
    autoload :Message
    autoload :Model

    def self.client
      Propono::Client.new do |config|
        config.application_name = Rails.app_class.parent.name.underscore
        config.logger = Rails.logger
      end
    end
  end
end

Fme::EnterpriseBus::Railtie
