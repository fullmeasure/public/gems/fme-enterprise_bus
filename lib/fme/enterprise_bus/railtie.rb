require 'fme/shoryuken'

module Fme
  module EnterpriseBus
    class Railtie < Rails::Railtie
      initializer 'setup constant based on env' do
        Fme::EnterpriseBus::ENTERPRISE_BUS_NAME = "#{Rails.env}_bus".freeze
      end
      initializer 'setup active job queue' do
        Fme::Shoryuken.add_queues %w[bus]
      end
      initializer 'setup enterprise jobs' do
        require_relative 'jobs'
      end
      config.after_initialize do
        ApplicationRecord.class_eval do
          include Fme::EnterpriseBus::Model::Destroyed
          include Fme::EnterpriseBus::Model::Saved
        end
      end
      rake_tasks { load 'fme/enterprise_bus/bus.rake' }
    end
  end
end
