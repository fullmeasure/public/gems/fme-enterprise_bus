require 'active_attr'

module Fme
  module EnterpriseBus
    class Message
      include ActiveAttr::Model

      attribute :type_name, default: 'Event'
      attribute :source_micro_service, default: Rails.app_class.parent.name
      attribute :namespace
      attribute :source_type
      attribute :creation_date, type: DateTime, default: -> { DateTime.now }
      attribute :payload
      attribute :assembly_name
      attribute :assembly_id

      def publish
        EnterpriseBus.client.publish(ENTERPRISE_BUS_NAME, as_json)
      end
    end
  end
end
