module Fme
  module EnterpriseBus
    module Model
      module ModelKlassName
        extend ActiveSupport::Concern

        class_methods do
          def add_enterprise_model_hook(hook_name, verb)
            send("after_#{hook_name}", "#{verb}_bus_message_publish".to_sym)

            delegate :publish, to: "#{verb}_bus_message".to_sym, prefix: true

            define_method "#{verb}_bus_message" do
              model_message.tap do |message|
                message.source_type = verb.capitalize
              end
            end
          end
        end

        included do
          def model_klass_name
            self.class.name
          end

          def model_message
            Message.new(
              type_name: 'Event',
              namespace: 'Model',
              payload: as_json,
              assembly_name: model_klass_name,
              assembly_id: id
            )
          end
        end
      end
    end
  end
end
