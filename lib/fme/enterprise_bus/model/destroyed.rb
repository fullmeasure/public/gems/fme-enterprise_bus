module Fme
  module EnterpriseBus
    module Model
      module Destroyed
        extend ActiveSupport::Concern

        included do
          include ModelKlassName

          add_enterprise_model_hook :destroy, :destroyed
        end
      end
    end
  end
end
