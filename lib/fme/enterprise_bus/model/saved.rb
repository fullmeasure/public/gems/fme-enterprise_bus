module Fme
  module EnterpriseBus
    module Model
      module Saved
        extend ActiveSupport::Concern

        included do
          include ModelKlassName

          add_enterprise_model_hook :save, :saved
        end
      end
    end
  end
end
