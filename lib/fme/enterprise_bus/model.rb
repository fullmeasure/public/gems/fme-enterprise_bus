module Fme
  module EnterpriseBus
    module Model
      extend ActiveSupport::Autoload

      autoload :Destroyed
      autoload :Saved
      autoload :ModelKlassName
    end
  end
end
