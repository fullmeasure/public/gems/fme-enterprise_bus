namespace :fme do
  namespace :enterprise_bus do
    desc 'listener for enterprise bus'
    task listener: :environment do
      Fme::EnterpriseBus.client.listen(Fme::EnterpriseBus::ENTERPRISE_BUS_NAME) do |message_hash|
        puts 'message', message_hash
        message = Fme::EnterpriseBus::Message.new(message_hash)
        Fme::EnterpriseBus::Jobs::BusJob.perform_later(message)
      end
    rescue Interrupt, SignalException => error
      Rails.logger.error error
    end
  end
end
