module Fme
  module EnterpriseBus
    module Jobs
      class RegisteredJobs
        include Singleton

        def initialize
          @jobs = []
        end

        def register_job(job_klass, message_hash)
          @jobs.push(job_klass: job_klass, message_pattern: message_hash)
        end

        def registered_jobs
          @jobs.clone.freeze
        end
      end
    end
  end
end
