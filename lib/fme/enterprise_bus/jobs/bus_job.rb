module Fme
  module EnterpriseBus
    module Jobs
      class BusJob < ActiveJob::Base
        queue_as :bus

        def perform(message)
          run_in_matching_jobs message
        end

        def run_in_matching_jobs(message)
          matching_jobs(message).each do |job|
            job.perform_later message
          end
        end

        def matching_jobs(message)
          matching_job_hashes(message).map do |job_klass:, **|
            job_klass
          end
        end

        def matching_job_hashes(message)
          Jobs.registered_jobs.select do |message_pattern:, **|
            (message_pattern.with_indifferent_access.to_a - message.as_json.to_a).empty?
          end
        end
      end
    end
  end
end
