module Fme
  module EnterpriseBus
    module Jobs
      class CleanupJob < ActiveJob::Base
        queue_as :bus

        def perform(message)
          @message = message
          Rails.logger.info "cleaning up #{column_name} on #{related_models.map(&:name)}"
          related_models.each do |klass|
            klass.where(args_to_destroy).destroy_all
          end
        end

        def args_to_destroy
          built_arguments = {
            column_name => @message.assembly_id
          }

          if organization_id.present?
            built_arguments[:organization_id] = organization_id
          end
          built_arguments
        end

        def organization_id
          @message.payload.with_indifferent_access.dig('organization_id')
        end

        def related_models
          Rails.application.eager_load!
          ApplicationRecord.descendants.select do |klass|
            klass.columns.map(&:name).include? column_name
          end
        end

        def column_name
          ActiveRecord::Base.send(:sanitize_sql_for_conditions, "#{@message.assembly_name.underscore}_id")
        end
      end
    end
  end
end
