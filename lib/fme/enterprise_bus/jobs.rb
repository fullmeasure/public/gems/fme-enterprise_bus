module Fme
  module EnterpriseBus
    module Jobs
      extend ActiveSupport::Autoload

      autoload :BusJob
      autoload :CleanupJob
      autoload :RegisteredJobs

      def self.register_job(job_klass, message_hash)
        RegisteredJobs.instance.register_job(job_klass, message_hash)
      end

      def self.registered_jobs
        RegisteredJobs.instance.registered_jobs
      end

      register_job(
        Jobs::CleanupJob,
        type_name: 'Event',
        namespace: 'Model',
        source_type: 'Destroyed'
      )
    end
  end
end
