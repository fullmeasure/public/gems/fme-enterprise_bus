require 'rails_helper'

RSpec.describe 'bus', type: [:task, :job] do
  describe 'listener' do
    let(:client_double) { double(:client, listen: 'listen') }
    let(:message) { Fme::EnterpriseBus::Message.new(namespace: 'message') }
    it 'enqueues worker' do
      expect(Rails.logger).to receive(:error)
      expect(client_double).to receive(:listen).with(Fme::EnterpriseBus::ENTERPRISE_BUS_NAME) do |&block|
        block.call(message.as_json)
        raise SignalException, 'HUP'
      end
      expect(Fme::EnterpriseBus).to receive(:client).and_return client_double
      expect(Fme::EnterpriseBus::Jobs::BusJob).to receive(:perform_later).with(message)
      Rake::Task['fme:enterprise_bus:listener'].invoke
    end
  end
end
