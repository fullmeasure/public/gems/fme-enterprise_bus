require 'rails_helper'

RSpec.describe Fme::EnterpriseBus::Jobs::CleanupJob, type: :job do
  let(:model) { Model.new(name: 'clean me', model_id: '432', organization_id: '52') }
  let(:model_not_to_delete) { Model.new(name: 'clean me', model_id: '432') }
  let(:message) do
    Fme::EnterpriseBus::Message.new(
      type_name: 'Event',
      namespace: 'Model',
      source_type: 'Destroyed',
      payload: {
        key: 'stuff',
        organization_id: '52'
      },
      assembly_name: 'Model',
      assembly_id: model.model_id
    )
  end
  before(:each) do
    Model.skip_callback(*%i(save after saved_bus_message_publish))
    Model.skip_callback(*%i(destroy after destroyed_bus_message_publish))
    model.save!
    model_not_to_delete.save!
  end
  after(:each) do
    Model.set_callback(*%i(save after saved_bus_message_publish))
    Model.set_callback(*%i(destroy after destroyed_bus_message_publish))
  end
  it 'deletes model' do
    expect {
      Fme::EnterpriseBus::Jobs::CleanupJob.perform_now(message)
    }.to change(Model, :count).by -1
  end
end
