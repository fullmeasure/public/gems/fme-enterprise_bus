require 'rails_helper'

RSpec.describe Fme::EnterpriseBus::Model do
  let(:client_double) { double(:client, publish: 'publish') }
  describe 'Saved' do
    let(:model) { Model.new(name: 'save me') }
    let(:expected_message) do
      Fme::EnterpriseBus::Message.new(
        type_name: 'Event',
        namespace: 'Model',
        source_type: :Saved,
        source_micro_service: 'Dummy',
        payload: model.as_json,
        assembly_name: 'Model',
        assembly_id: model.id
      )
    end
    before(:each) do
      Model.skip_callback(*%i(save after saved_bus_message_publish))
      model.save!
      Model.set_callback(*%i(save after saved_bus_message_publish))
      model.name = 'saved me'
      expect(Fme::EnterpriseBus).to receive(:client).and_return client_double
      expect(client_double).to receive(:publish).with(Fme::EnterpriseBus::ENTERPRISE_BUS_NAME, expected_message.as_json)
    end
    it { model.save! }
  end

  describe 'Destroyed' do
    let(:model) { Model.new(name: 'destroy me') }
    let(:expected_message) do
      Fme::EnterpriseBus::Message.new(
        type_name: 'Event',
        namespace: 'Model',
        source_type: :Destroyed,
        source_micro_service: 'Dummy',
        payload: model.as_json,
        assembly_name: 'Model',
        assembly_id: model.id
      )
    end
    before(:each) do
      Model.skip_callback(*%i(save after saved_bus_message_publish))
      model.save!
      expect(Fme::EnterpriseBus).to receive(:client).and_return client_double
      expect(client_double).to receive(:publish).with(Fme::EnterpriseBus::ENTERPRISE_BUS_NAME, expected_message.as_json)
    end
    after(:each) do
      Model.set_callback(*%i(save after saved_bus_message_publish))
    end
    it { model.destroy! }
  end
end
