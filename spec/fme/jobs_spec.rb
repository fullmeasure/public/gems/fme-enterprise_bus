require 'rails_helper'

RSpec.describe Fme::EnterpriseBus::Jobs do
  describe 'registered_jobs' do
    let(:expected_cleanup_job) do
      {
        job_klass: Fme::EnterpriseBus::Jobs::CleanupJob,
        message_pattern: {
          type_name: 'Event',
          namespace: 'Model',
          source_type: 'Destroyed'
        }
      }
    end
    it { expect(Fme::EnterpriseBus::Jobs.registered_jobs).to eq [expected_cleanup_job] }
  end
end
