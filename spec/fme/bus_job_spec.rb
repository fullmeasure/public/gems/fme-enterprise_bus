require 'rails_helper'

RSpec.describe Fme::EnterpriseBus::Jobs::CleanupJob, type: :job do
  let(:job) { Fme::EnterpriseBus::Jobs::CleanupJob }
  context 'when message matches registered partial hash' do
    let(:message) do
      Fme::EnterpriseBus::Message.new(
        type_name: 'Event',
        namespace: 'Model',
        source_type: 'Destroyed',
        payload: 'stuff',
        assembly_name: 'KlassName',
        assembly_id: '532'
      )
    end
    before(:each) do
      expect(job).to receive(:perform_later).with(message)
    end
    it { Fme::EnterpriseBus::Jobs::BusJob.perform_now(message) }
  end
  context 'when message does not match' do
    let(:message) do
      Fme::EnterpriseBus::Message.new(
        type_name: 'Event',
        namespace: 'Model',
        source_type: 'Saved',
        payload: 'stuff',
        assembly_name: 'KlassName',
        assembly_id: '532'
      )
    end
    before(:each) do
      expect(job).not_to receive(:perform_later).with(message)
    end
    it { Fme::EnterpriseBus::Jobs::BusJob.perform_now(message) }
  end
end
