require 'rails_helper'

RSpec.describe Fme::EnterpriseBus do
  it 'has a version number' do
    expect(Fme::EnterpriseBus::VERSION).not_to be nil
  end

  describe 'ENTERPRISE_BUS_NAME' do
    it { expect(Fme::EnterpriseBus::ENTERPRISE_BUS_NAME).to eq 'test_bus' }
  end

  describe 'AwsConfig' do
    it 'monkey patch Propono aws config to not require credentials' do
      expect(Propono::AwsConfig.new({}).aws_options).to eq({})
    end
  end

  describe 'client' do
    let(:client) { Fme::EnterpriseBus.client }
    it { expect(client.config.application_name).to eq Rails.app_class.parent.name.underscore }
    it { expect(client.config.logger).to eq Rails.logger }
  end
end
