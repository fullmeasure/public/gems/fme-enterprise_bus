class AddModel < ActiveRecord::Migration[5.2]
  def change
    create_table :models do |t|
      t.string :name
      t.belongs_to :model
      t.integer :organization_id
    end
  end
end
