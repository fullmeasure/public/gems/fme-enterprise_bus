RSpec.configure do |config|
  config.include ActiveJob::TestHelper, type: :job
  config.before(:suite) do
    ActiveJob::Base.queue_adapter = :test
  end
  config.after(:each, type: :job) do
    clear_enqueued_jobs
  end
end
